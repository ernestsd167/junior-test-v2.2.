<?php include("includes/a_config.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php");?>
    <script>
    //Function that shows the special atribute
    function showAppropriate()
    {
        var val = document.getElementById('product_type').value;
        if(val == 'dvd'){
            document.getElementById('chosen_option').innerHTML = 'Size(in MB): <input type="text" class="form-control my-1" name="special_atr">';
        }
        else if(val == 'book'){
            document.getElementById('chosen_option').innerHTML = 'Weight(in Kg): <input type="text" class="form-control" name="special_atr">';
        }
        else if(val == 'furniture'){
            document.getElementById('chosen_option').innerHTML = 'Dimensions(HxWxL format): <input type="text" class="form-control" name="special_atr">';
        }
        else{
            document.getElementById('chosen_option').innerHTML = '';
        }
    }
    </script>
    <?php
    $sku_err = $name_err = $price_err = $special_err = "";
    //Validates the input and counts how many errors
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $error_count = 0;
        if (empty($_POST["SKU"])){
            $sku_err = "SKU is required";
            $error_count++;
        }
        if(empty($_POST["name"])){
            $name_err = "Name is required";
            $error_count++;
        }
        if(empty($_POST["price"])){
            $price_err = "Price is required";
            $error_count++;
        }
        if(empty($_POST["special_atr"])){
            $special_err = "The special atribute is required";
            $error_count++;
        }
    }
    $sent = 0; //Variable to know if the form successfully has been sent
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        //Checking for errors
        if($error_count == 0)
        {
            $mysql = new mysql;
            $post = new post;
            $post->__construct();
            $sql = "INSERT INTO Posts (SKU, Name, Price, pType, Special) VALUES (?, ?, ?, ?, ?)";
            //OOP style used to prepare SQL statement(Protection against SQL Injection)
            $post->prepareStmt($sql, $mysql);
            //ssds = string, string, double, string, string (specifies the types for the table columns)
            $post->executeStmt("ssdss");
            $post->__destruct();
            $mysql->close();
            $sent++;
        }
    }
    ?>
</head>
<body>
<?php include("includes/navigation.php"); ?>

<div class="container">
    <div class="row border-bottom border-dark my-4">
        <h1>Add product</h1>
    </div>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <div class="form-group">
        SKU: <input type="text" name="SKU" class="form-control">
        <span><?php echo $sku_err;?></span>
        </div>
        <div class="form-group">
        Name: <input type="text" name="name" class="form-control">
        <span><?php echo $name_err;?></span>
        </div> 
        <div class="form-group">
        Price: <input type="number" min="0.01" class="form-control" step="0.01" max="9999.99" name="price">
        <span><?php echo $price_err;?></span>
        </div> 
        <select name="product-type" class="browser-default custom-select" id="product_type" onchange="showAppropriate()">
            <option value="">Product type?</option>
            <option value ="dvd">DVD-disc</option>
            <option value = "book">Book</option>
            <option value = "furniture">Furniture</option>
        </select>
        <div class="form-group">
        <span id="chosen_option"></span>
        <span><?php echo $special_err;?></span>
        </div>
        <input type="submit" class="btn btn-secondary">
    </form>
    <?php
        if ($sent>0)
        {
            echo '<div class="my-4"><h2>Form submitted</h2></div>';
        } 
    ?>
</div>

</body>
</html>
<?php include("includes/a_config.php");?>

<!DOCTYPE html>
<html lang="en">
  <head>
	<?php
        include("includes/head.php");
        $mysql = new mysql;
		$mysql->connect("localhost", "root", "", "myDB");
		if(isset($_POST["submit1"]))
		{
			$box=$_POST["num"];
			while(list($key, $val) = @each ($box)){
				$sql = "DELETE FROM Posts WHERE id=$val";
				$mysql->cQuery($sql);
			}
		}
	?>
	<style>
	.custom:hover {
		background-color: rgb(224, 224, 224);
	}
	</style>
  </head>
  <body>
	<!-- Page Content -->
	<?php include("includes/navigation.php"); ?>
		
		<div class="container">
		<div class="row border-bottom border-dark my-4">
			<h1>Product List</h1>
		</div>
			<?php
                $mysql = new mysql;
                $mysql->connect("localhost", "root", "", "myDB");
				$sql = "SELECT ID, SKU, Name, Price, pType, Special FROM posts";
                $result = $mysql->getResult($sql);
				function specAtr($pType)
				{
					$typeMsg = "";
					switch ($pType){
						case "book":
							$typeMsg = "Weight: ";
							break;
						case "dvd":
							$typeMsg = "Size: ";
							break;
						case "furniture":
							$typeMsg = "Dimension: ";
							break;
						default:
							$typeMsg = "";
							break;
					}
					return $typeMsg;
				}
				//Check if there are any rows
				if($result->num_rows > 0) 
				{
					//Getting post count
					$num_of_rows = $result->num_rows;
					while($num_of_rows > 0) 
					{
						$count = 0;
						echo '<form name="form1" action="" method="post">';
						echo '<div class="row text-center my-4">';
						
						while($count < 3)
						{	
							$row = $result->fetch_assoc();
							if($row == NULL)
							{
								break;
							}
							$type = $row["pType"];
							$msg = specAtr($type);
							//Displaying rows
							echo '<div class="col-md-3 border border-dark mx-4 custom">
							<input type="checkbox" name="num[]" value="'.$row["ID"].'">
							<br>'.$row["SKU"].'
							<br>' .$row["Name"].'
							<br>' .$row["Price"].' $
							<br>' .$msg. ' ' .$row["Special"].'
							</div>';
							$count++;
						}
						echo '</div>';
						$num_of_rows -= 3; //Substracting the rows that already have been displayed for the counter
					}
				}
				$mysql->close();
			?>
			<div class="row border-top border-dark my-2">
				<input type="submit" class=" btn btn-secondary my-2" name="submit1" value="Delete selected">
			</div>
		</div>
			
  </body>
</html>
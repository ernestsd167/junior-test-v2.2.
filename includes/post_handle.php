<?php

class post
{
    var $SKU;
    var $name;
    var $price;
    var $pType;
    var $specialAtr;
    var $stmt;

    public function __construct()
    {
        $this->SKU = $_POST["SKU"];
        $this->name = $_POST["name"];
        $this->price = $_POST["price"];
        $this->pType = $_POST["product-type"];
        $this->specialAtr = $_POST["special_atr"];
    }
    public function prepareStmt($sql, $mysql)
    {
        $mysql->connect("localhost", "root", "", "myDB");
        $this->stmt = $mysql->getStmt($sql);
    }
    public function executeStmt($types)
    {
        $this->stmt->bind_param($types, $this->SKU, $this->name, $this->price, $this->pType, $this->specialAtr);
        $this->stmt->execute();
    }
    public function __destruct()
    {
        $this->SKU = NULL;
        $this->name = NULL;
        $this->price = NULL;
        $this->pType = NULL;
        $this->specialAtr = NULL;
        $this->stmt = NULL;
    }
}





?>
<title><?php print $PAGE_TITLE;?></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<?php
    // Create Database if not created
    include("classes.php");
    // Create connection
    $mysql = new mysql;
    $mysql->createDatabase("localhost", "root", "", "myDB");
    $mysql->connect("localhost", "root", "", "myDB");
    $sql = "CREATE TABLE IF NOT EXISTS Posts (
        ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        SKU VARCHAR(30) NOT NULL,
        Name VARCHAR(30) NOT NULL,
        Price DOUBLE(6, 2) NOT NULL,
        pType VARCHAR(30) NOT NULL, 
        Special VARCHAR(30) NOT NULL
        )";
    $mysql->checkTable("posts");
    $mysql->createTable($sql);
    $mysql->close();
?> 
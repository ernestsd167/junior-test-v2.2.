<?php

class mysql
{
    var $servername;
    var $username;
    var $password;
    var $database;
    var $dbCon;

    public function createDatabase($set_servername, $set_username, $set_password, $db)
    {
        $this->servername = $set_servername;
        $this->username = $set_username;
        $this->password = $set_password;
        $this->database = $db;
        $this->dbCon = mysqli_connect($this->servername, $this->username, $this->password);
        $sql = 'CREATE DATABASE IF NOT EXISTS $database';
        if($this->dbCon->query($sql) !== TRUE)
        {
            echo "Error creating database: " . $con->error;
        }
        $this->dbCon->close();
    }
    public function connect($set_servername, $set_username, $set_password, $set_database)
    {
        $this->servername = $set_servername;
        $this->username = $set_username;
        $this->password = $set_password;
        $this->database = $set_database;
        $this->dbCon = mysqli_connect($this->servername, $this->username, $this->password, $this->database);
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
    }
    public function cQuery($sql)
    {
        $this->dbCon->query($sql) or die($this->dbCon->error);
    }
    public function checkTable($table_name)
    {
        $val = $this->dbCon->query("SELECT 1 FROM `$table_name` LIMIT 1");
        if($val == True)
        {
            return false;
        }
    }
    public function createTable($sql)
    {
        if($this->dbCon->query($sql) !== TRUE)
        {
            echo "Error creating table: " . $this->dbCon->error;
        }
    }
    public function close()
    {
        return $this->dbCon->close();
    }
    public function getResult($sql)
    {
        return $this->dbCon->query($sql);
    }
    public function getStmt($sql)
    {
        return $this->dbCon->prepare($sql);
    }
}


?>